<?php

/**
 * Implementation of hook_ca_predicate().
 */
function uc_matrix_ca_predicate() {
  $predicates = array();

  $predicates['uc_matrix_pre_apply_commission'] = array(
    '#title' => t('Matrix pre-apply new commissions.'),
    '#trigger' => 'uc_affiliate2_pre_apply_commission', // uc_pull_trigger('this param', ...)
    '#class' => 'notification',
    '#status' => 1,
    '#weight' => -2, // make sure we run 1st
    '#conditions' => array(), // empty defaults to TRUE
    '#actions' => array(
      array(
        '#name' => 'uc_matrix_matrix',
        '#title' => t('Force the new purchaser in the affiliate matrix as defined in the administration screens.'),
        '#argument_map' => array(
          'order' => 'order',
          'affiliate_identifier' => 'affiliate_identifier',
        ),
      ),
    ),
  );

  return $predicates;
}

/**
 * Implementation of hook_ca_action().
 */
function uc_matrix_ca_action() {
  $actions['uc_matrix_matrix'] = array(
    '#title' => t('Override the commission behavior with UC Matrix.'),
    '#category' => t('Forced Matrix'),
    '#callback' => 'uc_matrix_force',
    '#arguments' => array(
      'order' => array(
        '#entity' => 'uc_order',
        '#title' => t('Order'),
      ),
      'affiliate_identifier' => array(
        '#entity' => 'uc_affiliate2_identifier',
        '#title' => t('Affiliate Identifier'),
      ),
    ),
  );

  return $actions;
}

/**
 * Ensure that the matrix is "properly" forced.
 */
function uc_matrix_force(&$order, &$affiliate_id) {
  watchdog('uc_matrix', 'We got in uc_matrix_force!!! ['.$order->order_id.'] ['.$affiliate_id.']');
  drupal_set_message('We got in uc_matrix_force!!! ['.$order->order_id.'] ['.$affiliate_id.']', 'error');
  
}

// vim: ts=2 sw=2 et syntax=php
