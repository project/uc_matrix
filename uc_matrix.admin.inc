<?php

/** @file
 *
 * Forms to let users edit UC Matrix data.
 */

/**
 * Matrix Menu, build array here.
 */
function _uc_matrix_menu() {
  $itmes = array();

  $items['admin/store/affiliate/matrix'] = array(
    'title' => 'Forced matrix',
    'description' => 'Manage the affiliate matrix extensions.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_matrix_settings_form'),
    'access arguments' => array('administer matrix'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'uc_matrix.admin.inc',
  );

  // root user auto-complete feature; we make sure that the user
  // has permission to represent the root user!
  $items['admin/store/affiliate/autocomplete/matrix_user'] = array(
    'title' => 'Autocomplete feature upgrades',
    'page callback' => 'uc_matrix_user_autocomplete',
    // access not properly checked on callbacks (see function)
    'access arguments' => array('administer matrix'),
    'type' => MENU_CALLBACK,
    'file' => 'uc_matrix.user.autocomplete.inc',
  );

  return $items;
}

/**
 * Create the settings form so users can edit the Matrix settings.
 */
function uc_matrix_settings_form() {
  $form = array();

  $form['uc_matrix'] = array(
    '#type' => 'fieldset',
    '#title' => t('Forced matrix'),
    '#description' => t('Global settings for your forced matrix.')
      . '<br /><br /><span style="color: red;">' . t('WARNING:') . '</span> '
        . t('Changing your matrix when you already have customers will not re-shuffle the existing connections, but it may cause some problems.') . '<br /><br />'
      . t('The forced matrix settings are found in several places.') . '<br />'
      . t('The !user_roles settings include information for each role.', array('!user_roles' => l('user roles', 'admin/user/roles'))) . '<br />'
      ,
  );

  $form['uc_matrix']['uc_matrix_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => 'Use the Forced Matrix system',
    '#description' => 'You may turn off the Forced Matrix system without actually disabling it. By default, it is turned on (checked.)',
    '#default_value' => variable_get('uc_matrix_enabled', TRUE),
  );

  return system_settings_form($form);
}

/**
 * Form presented in the User roles.
 */
function _uc_matrix_roles_form(&$form, $form_state) {
  if (isset($form['rid']['#value'])) {
    $rid = $form['rid']['#value'];
    $sql = "SELECT * FROM {uc_matrix_roles} WHERE rid = %d";
    $result = db_query($sql, $rid);
    $row = db_fetch_array($result);
  }
  else {
    // user is adding a new role
    $row = array(
      'enabled' => FALSE,
      'root_user' => '',
      'maximum_per_level' => 0,
      'maximum_auto_add' => 0,
      'maximum_new_add' => 0,
      'force_maximum_new_add' => FALSE,
    );
  }

  $form['uc_matrix'] = array(
    '#type' => 'fieldset',
    '#title' => t('Forced matrix'),
    '#description' => t('Settings to create your forced matrix.')
      . '<br /><span style="color: red;">' . t('WARNING:') . '</span> '
        . t('Changing your matrix when you already have customers will not re-shuffle the existing connections, but it may cause some problems.'),
    '#collapsible' => TRUE,
    '#collapsed' => !$row['enabled'],
  );

  $form['uc_matrix']['uc_matrix_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable'),
    '#description' => t('Turn on the Forced Matrix system for users and products in this role. The default is off (unchecked.)'),
    '#default_value' => $row['enabled'],
  );

  $root_name = NULL;
  if ($row['root_user']) {
    $account = user_load(array('uid' => $row['root_user']));
    if ($account) {
      $root_name = $account->name;
    }
  }
  $form['uc_matrix']['uc_matrix_root_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Root User'),
    '#description' => t('Specify who gets all the users who are not coming to your website through an affiliate. Keep this empty to avoid this feature. People without an affiliate are made root themselves.'),
    '#default_value' => $root_name,
    '#autocomplete_path' => 'admin/store/affiliate/autocomplete/matrix_user',
  );

  $form['uc_matrix']['uc_matrix_maximum_per_level'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum agents below another'),
    '#description' => t('Specify the maximum number of agents that can purchase a product right below a forced matrix agent. Use 0 to remove the maximum. If the maximum is reached, new agents are added below at the next level of agents. Only buying users are viewed as forced matrix agents. New comers are always added under the current agent the information will be saved in the original_aid field of the uc_matrix_members table.'),
    '#default_value' => $row['maximum_per_level'],
  );

  $form['uc_matrix']['uc_matrix_maximum_auto_add'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum agents automatically added below another'),
    '#description' => t('Specify the maximum number of agents added automatically at the next level before we start filling up the next level down. The automatic additions are done up to the maximum number of levels offered by the products being purchased. Otherwise, this value is ignored. Use 0 to avoid this effect.'),
    '#default_value' => $row['maximum_auto_add'],
  );

  $form['uc_matrix']['uc_matrix_maximum_new_add'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum non-affiliated agents automatically added below others'),
    '#description' => t('Specify the maximum number of non-affiliated agents to add automatically at the next available level. When you specify a root user, he becomes the de-facto affiliate. However, to offer a better chance to new users to get an agent below them, this feature forces a maximum number of agents at any level before such users are added at the next level.'),
    '#default_value' => $row['maximum_new_add'],
  );

  $form['uc_matrix']['uc_matrix_force_maximum_new_add'] = array(
    '#type' => 'checkbox',
    '#title' => t('Force the Maximum non-affiliate agents limit'),
    '#description' => t('This flag has no effect if you do not specify a root agent. When this flag is set and you define a root agent, instead of filling up the root agent tree, the system continues adding users below others who still did not reach the maximum.'),
    '#default_value' => $row['force_maximum_new_add'],
  );

  // adjust weights for those buttons or they will appear before our entries.
  $form['submit']['#weight'] = 10;
  if (isset($form['delete'])) {
    $form['delete']['#weight'] = 11;
  }

  $form['#validate'][] = '_uc_matrix_roles_form_validate';
  $form['#submit'][] = '_uc_matrix_roles_form_submit';
}

/**
 * Validate the Role information if Enabled is 1.
 */
function _uc_matrix_roles_form_validate($form, $form_state) {
  $values = &$form_state['values'];
  if ($values['uc_matrix_enabled']) {
    // test only if enabled, otherwise ignore because it is likely that some
    // errors would otherwise be generated.

    // make sure the root user is properly defined
    $root_user_name = $values['uc_matrix_root_user'];
    if ($root_user_name) {
      $root_user = user_load(array('name' => $root_user_name));
      if (!is_object($root_user)) {
        form_set_error(
          'uc_matrix_root_user',
          t('Could not find user "@name". Please try again with the name of an existing user.',
            array('@name' => $root_user_name)),
          'error'
        );
      }
    }

    if ($values['uc_matrix_maximum_per_level'] <= 0) {
      form_set_error('uc_matrix_maximum_per_level', t('The total maximum number of agents must be at least 1, and most probably 2 or more.'));
    }
    else {
      if ($values['uc_matrix_maximum_auto_add'] < 0) {
        form_set_error('uc_matrix_maximum_auto_add', t('The number of agents to auto-add cannot be negative.'));
      }
      elseif ($values['uc_matrix_maximum_auto_add'] > $values['uc_matrix_maximum_per_level']) {
        form_set_error('uc_matrix_maximum_auto_add', t('The number of agents to auto-add cannot be larger than the maximum allowed at a level.'));
      }
      if ($values['uc_matrix_maximum_new_add'] < 0) {
        form_set_error('uc_matrix_maximum_new_add', t('The number of non-affiliated agents to auto-add cannot be negative.'));
      }
      elseif ($values['uc_matrix_maximum_new_add'] > $values['uc_matrix_maximum_per_level']) {
        form_set_error('uc_matrix_maximum_new_add', t('The number of non-affiliated agents to auto-add cannot be larger than the maximum allowed at a level.'));
      }
      // warn about potential error
      if ($values['uc_matrix_maximum_new_add'] > $values['uc_matrix_maximum_auto_add']) {
        drupal_set_message(t('The number of non-affiliated agents to auto-add is larger than the number of affiliated agents to auto-add. Is this a mistake?'), 'warning');
      }
    }
  }
}

/**
 * Save the current data in our table.
 */
function _uc_matrix_roles_form_submit($form, &$form_state) {
  global $db_type;

  $values = &$form_state['values'];
  $rid = $form['rid']['#value'];
  $root_user_name = $values['uc_matrix_root_user'];
  if ($root_user_name) {
    $root_user = user_load(array('name' => $root_user_name));
    $root_uid = $root_user->uid;
  }
  else {
    $root_uid = 0;
  }
  $sql = "UPDATE {uc_matrix_roles}"
    . " SET enabled = %d, root_user = %d, maximum_per_level = %d,"
      . " maximum_auto_add = %d, maximum_new_add = %d, force_maximum_new_add = %d"
    . " WHERE rid = %d";
  db_query($sql, $values['uc_matrix_enabled'], $root_uid,
    $values['uc_matrix_maximum_per_level'], $values['uc_matrix_maximum_auto_add'],
    $values['uc_matrix_maximum_new_add'], $values['uc_matrix_force_maximum_new_add'], $rid);
  if (db_affected_rows() === 0) {
    $skip = 0;
    if ($db_type == 'mysql' || $db_type == 'mysqli') {
      $skip = db_query("SELECT 1 FROM {uc_matrix_roles} WHERE rid = %d", $rid);
    }
    if (!$skip) {
      $sql = "INSERT INTO {uc_matrix_roles} (rid, enabled, root_user, maximum_per_level,"
          . " maximum_auto_add, maximum_new_add, force_maximum_new_add)"
        . " VALUES (%d, %d, %d, %d, %d, %d, %d)";
      db_query($sql, $rid, $values['uc_matrix_enabled'], $root_uid,
        $values['uc_matrix_maximum_per_level'], $values['uc_matrix_maximum_auto_add'],
        $values['uc_matrix_maximum_new_add'], $values['uc_matrix_force_maximum_new_add']);
    }
  }
}

// vim: ts=2 sw=2 et syntax=php
