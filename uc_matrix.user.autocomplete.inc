<?php

/** @file
 *
 * Auto-complete user name in Roles form.
 */

/**
 * Auto-complete the string to a user name with only users
 * who have permission to act as an affiliate.
 */
function uc_matrix_user_autocomplete($string) {
  if (!$string) {
    echo drupal_to_js(array());
    exit();
  }

  // make 100% sure that user has permission!
  if (!user_access('administer matrix')) {
    drupal_access_denied();
    return;
  }

  // don't use a range because it won't work if many users do not have
  // the right to act as an affiliate.
  //
  // XXX -- on sites with a large number of users it won't work well...
  //        we may want to write a full SELECT with tests of who has
  //        the 'act as affiliate' permission at once.
  $count = 0;
  $matches = array();
  $result = db_query("SELECT uid FROM {users} WHERE LOWER(name) LIKE LOWER('%s%%') AND uid NOT IN (0, 1)", $string);
  while ($count < 10 && $u = db_fetch_object($result)) {
    $account = user_load(array('uid' => $u->uid));
    if (user_access('act as affiliate', $account)) {
      $matches[$account->name] = check_plain($account->name);
      ++$count;
    }
  }

  echo drupal_to_js($matches);
  exit();
}

// vim: ts=2 sw=2 et syntax=php
